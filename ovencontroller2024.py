# %%
import time
import asyncio

import serial
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from mplnow import FigDrawing
from numpy.typing import NDArray, ArrayLike
from IPython.display import display

matplotlib.use("module://ipympl.backend_nbagg")

SERIAL_PORT = "com3"
ser = serial.Serial(SERIAL_PORT, baudrate=38400)

fig = FigDrawing()

# %%
# Define Temperature Profile (time, temp)
P_gain = 10

test_profile = np.array([[0, 70], [20, 35], [40, 70]])
# copied from http://www.smithsconnectors.com/us/library/full-library/pcb-connectors/dovetail-sac305-reflow-solder-profile
# for "densly populated boards"
led_profile = [
    [0, 40],
    [90, 120],
    [180, 140],
    [270, 220],
    [280, 220],
    [360, 80],
]


# copied from http://www.smithsconnectors.com/us/library/full-library/pcb-connectors/dovetail-sac305-reflow-solder-profile
# for "densly populated boards"
led_free_profile = [
    [0, 40],
    [20, 40],
    [130, 170],
    [320, 170],
    [370, 215],
    [385, 210],
    [460, 40],
]

off_profile = [
    [0, 0],
    [300, 0],
]

warmup_profile = [
    [0, 80],
    [120, 80],
]

cypress_profile = [
    [0, 30],
    [120, 150],
    [230, 200],
    [245, 217],
    [305, 260],
    [340, 260],
    [365, 217],
    [465, 30],
]


class TempProfile:
    def __init__(self, profile: ArrayLike):
        profile = np.asarray(profile)
        self.t = profile[:, 0]
        self.T = profile[:, 1]

    def getT(self, time):
        return np.interp(time, self.t, self.T).item(0)


# %%
profile = TempProfile(off_profile)
profile = TempProfile(cypress_profile)
t0 = time.time()
measure_ts = []
measure_Ts = []


# %%
async def loop():
    ser.write(b"get_temp\n")
    l = ser.readline()
    temp = float(l)

    if temp > 0:
        measure_ts.append(time.time())
        measure_Ts.append(temp)

    t = np.asarray(measure_ts) - t0
    T = np.asarray(measure_Ts)

    now_t = t[-1] if len(t) > 0 else 0
    target_T = profile.getT(now_t)

    if temp > target_T:
        temp_error = temp - target_T
        fan_speed = temp_error * 10 + 80
        if fan_speed > 255:
            fan_speed = 255

        line = b"set_fan %f\n" % fan_speed
        ser.write(line)

        ser.write(b"heater_off\n")
    else:
        ser.write(b"set_fan 0\n")
        ser.write(b"heater_on\n")

    with fig.begin():
        (ax,) = fig.subplots(1, 1)
        ax.axvline(now_t, ls="--", c="#eee")
        ax.plot(t, T, ".")

        ax.plot(profile.t, profile.T, "-g")
        ax.ax.set_title(f"Target: {target_T:.1f}°C, Current: {temp:.1f}°C")

        ax.autoscale(enable=True, axis="both")
        ax.set_xlabel("Time (s)")
        ax.set_ylabel("Temperature (°C)")


# %%
display(fig.fig.canvas)

go = True

async def task():
    while go:
        await loop()
        await asyncio.sleep(0.2)


task = asyncio.get_event_loop().create_task(task())

# %%

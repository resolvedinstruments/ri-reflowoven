  /*
 * A simple thermometer made with an NTC 3950 100k thermister.
 * 
 * wired up like http://reprap.org/wiki/Thermistor#EPCOS_100K_Thermistor_.28B57560G104F.29
 * with a 4.6k resistor in series with the 100k thermister.
 * 
 * Use "B parameter equation" to calculate temperature from resistance
 * https://en.wikipedia.org/wiki/Thermistor#B_or_.CE.B2_parameter_equation
 * 
 */
const int fan = 10;
const int heater = 9;

void setup() {
  pinMode(fan, OUTPUT);
  pinMode(heater, OUTPUT);
  analogWrite(fan, 0);
  digitalWrite(heater, 0);
  
  Serial.begin(38400);
}

const float k = 273.1;
const float B = 3950;
const float r_inf = 100e3 * exp(-B / 298.1);

float read_temperature()
{
    int temp_val = analogRead(A0);
    float a = temp_val / 1024. * 1;
    float r = (1. - a) / a * 4.6e3;
    float T = B / log(r / r_inf) - k;
    return T;
}

float temp = 0.0;

String line = "";
String cmd = "";
String val = "";

void loop() {
    // check serial port for commands
    while (Serial.available() > 0) {
        line = Serial.readStringUntil('\n');
        line.trim();
        
        int i = line.indexOf(' ');
        if (i < 0) {
            cmd = line;
            val = "";
        } else {
            cmd = line.substring(0, i);
            val = line.substring(i);
        }
        

        if (cmd == "get_temp") {
            temp = read_temperature();
            Serial.println(temp);
        } else
        if (cmd == "set_fan") {
          analogWrite(fan, val.toInt());
        } else
        if (cmd == "heater_on") {
          digitalWrite(heater, 1);
        } else
        if (cmd == "heater_off") {
          digitalWrite(heater, 0);
        }
    }
}
